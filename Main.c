/*
============================================================================
Name        : Risiko.c
Author      : Orange_dugongo
Version     : alpha 0.2
Copyright   : CC BY-SA 3.0 IT
Description : Simulatore di un turno del Risiko!
============================================================================
 */

#include "Risiko.h"

int main(){

srand(time(NULL));

  int a[3], d[3];//array per i dadi della difesa e dell'attacco.
  int win=0, lose=0, reduci=0;
  const int n=100;
  int ta;//truppe dell'attacco.
  int td;//Truppe della difesa.
  int i;

  do{
    printf("Di quante truppe dispone la difesa: ");
    scanf("%d", &td);
  }while(td<=0);

  do{
    printf("Di quante truppe dispone l'attacco: ");
    scanf("%d", &ta);
  }while(--ta<=0);//Viene Sottratta una truppa che resta sul territorio.

  for(i=0; i<n; i++){
    //Rippristino delle condizioni iniziali all'inizio di ogni turno.
    t.ta=ta;
    t.td=td;
    t.end=0;

    while(!t.end)
      Turno(a, d);

    if(t.ta!=0){
      win++;
      reduci+=t.ta+1;//Viene aggiunta la truppa sotratta precedentemente.
    }else
      lose++;
  }
    win>0?printf("Hai il %d\\%d di vincere con %d truppe rimanenti.", win, n, reduci/win):printf("Vai incontro ad una sconfitta certa.");
  __fpurge(stdin);
  getchar();
}
