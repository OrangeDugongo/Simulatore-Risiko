/*
============================================================================
Name        : Risiko.c
Author      : Orange_dugongo
Version     : alpha 0.2
Copyright   : CC BY-SA 3.0 IT
Description : Simulatore di un turno del Risiko!
============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdio_ext.h>

typedef struct{
  int ta;//Truppe dell'attacco.
  int td;//Truppe della difesa.
  int end;//Turno concluso.
} Nodo;

Nodo t;

void Swap(int j, int i[]){
  int temp;
  temp=i[j];
  i[j]=i[j+1];
  i[j+1]=temp;
}

void Sort(int i[]){
  int j, n=3;
  while(--n>0)
    for(j=0;j<n;j++)
      if(i[j]<i[j+1])
        Swap(j, i);
}

void Svuota(int a[]){
  int i;
  for(i=0;i<3;i++)
    a[i]=0;
}

int Min(int a, int b){
  int min=b;
  if(a<=b)
    min=a;
  return(min);
}

int Load(int l[], int truppe){
  int n, i;
  Svuota(l);
  n=truppe>3?3:truppe;
  for(i=0;i<n;i++)
    l[i]=rand()%6+1;

  Sort(l);
  return(n);
}

void Turno(int a[], int d[]){
  int n, i;
  n=Min(Load(a, t.ta), Load(d, t.td));
  for(i=0;i<n;i++)
    if(a[i]>d[i])
      t.td--;
    else
      t.ta--;

  if(t.ta<1 || t.td<1)
    t.end=1;
}
