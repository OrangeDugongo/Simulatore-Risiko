/*
============================================================================
Name        : Risiko.c
Author      : Orange_dugongo
Version     : alpha 0.2
Copyright   : CC BY-SA 3.0 IT
Description : Simulatore di un turno del Risiko!
============================================================================
 */

#include "Risiko.c"

//Tira i dadi.
int Load(int [], int);

//Scambia di posizioni due numeri.
void Swap(int, int []);

//Ordina un array in ordine decrescente.
void Sort(int []);

//Simula un attacco finche la difesa o l'attacco finiscono le proprie truppe.
void Turno(int [], int []);

//Ripristina a zero i dadi.
void Svuota(int []);

//Restituisce il minore tra due numeri.
int Min(int, int);
